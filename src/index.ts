import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import cors from 'cors'
import passport from 'passport'

const app = express()

app.use(bodyParser())
app.use(morgan("tiny"))
app.use(cors())

// http.createServer(app).listen(() => {})
app.listen(() => {
  console.log("Server started")
})